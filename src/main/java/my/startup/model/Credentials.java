package my.startup.model;

import lombok.Data;

/**
 * Store information about user credentials.
 * @see  User to more information about user.
 * @author alek
 */
@Data
public class Credentials {

    /** User login */
    private String login;

    /** User password */
    private String password;

}
