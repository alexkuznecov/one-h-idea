package my.startup.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * This class used to store basic information about user.
 * @see Credentials to information about passwords.
 * @author alek
 */
@Data
@Entity
@Table(name = "user")
public class User {

    /** User id from database*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /** User first name */
    @Column(name = "name")
    private String name;

    /** User last name */
    @Column(name = "surname")
    private String surname;

    /** User login */
    @Column(name = "login")
    private String login;

    /** User email */
    @Column(name = "email")
    private String email;

    /** User date of birth */
    @Column(name = "dob")
    private String dob;

}
