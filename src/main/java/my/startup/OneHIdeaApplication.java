package my.startup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneHIdeaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneHIdeaApplication.class, args);
	}
}
